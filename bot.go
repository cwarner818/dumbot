package main

import (
	"flag"
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

// Variables used for command line parameters
var (
	DB          *bolt.DB
	Token       string
	WelcomeChan string
	PingChan    string

	PingSayings = []string{
		":sparkling_heart: :regional_indicator_p: :regional_indicator_i: :regional_indicator_n: :regional_indicator_g: :sparkling_heart: :regional_indicator_p: :regional_indicator_o: :regional_indicator_n: :regional_indicator_g: :sparkling_heart: :regional_indicator_l: :regional_indicator_o: :regional_indicator_t: :regional_indicator_t: :regional_indicator_e: :regional_indicator_r: :regional_indicator_y: :sparkling_heart:",
		":sparkling_heart: :sparkles: :sparkling_heart: :sparkles: :sparkling_heart: :sparkles: :sparkling_heart: :sparkles: :sparkling_heart: :sparkles:",
		":regional_indicator_f: :punch: :sparkles: :regional_indicator_u: :punch: :sparkles: :regional_indicator_c: :punch: :sparkles: :regional_indicator_k: :punch: :sparkles: :regional_indicator_u: :hugging:",
		":rainbow: :regional_indicator_a: :regional_indicator_y: :regional_indicator_y: :regional_indicator_y: :regional_indicator_y: :regional_indicator_y: :rainbow: :regional_indicator_f: :regional_indicator_a: :regional_indicator_g: :regional_indicator_g: :regional_indicator_o: :regional_indicator_t: :rainbow:",
		":thinking: THINKING REAL HARD :eggplant: ABOUT THAT DICKKK :kissing_heart: :kiss:",
		":kissing_heart: :heart_eyes: :regional_indicator_y: :regional_indicator_o: :regional_indicator_u: :sparkling_heart: :ok_hand: :regional_indicator_w: :regional_indicator_o: :regional_indicator_n: :kissing_heart: :heart_eyes:",
		":alarm_clock: :sparkles: :middle_finger: :regional_indicator_i: :regional_indicator_t: :regional_indicator_s: :sparkling_heart: :regional_indicator_t: :regional_indicator_i: :regional_indicator_m: :regional_indicator_e: :upside_down: :kissing_heart: :alarm_clock:",
		":regional_indicator_t: :sun_with_face: :regional_indicator_i: :sun_with_face: :regional_indicator_c: :sun_with_face: :regional_indicator_k: :sun_with_face: :weary: :sparkling_heart: :regional_indicator_t: :sparkling_heart:  :regional_indicator_o: :sparkling_heart: :regional_indicator_c: :sparkling_heart: :regional_indicator_k: :sparkling_heart: :b: :sparkling_heart: :regional_indicator_i: :sparkling_heart: :regional_indicator_t: :sparkling_heart: :regional_indicator_c: :sparkling_heart: :regional_indicator_h: :weary:",
		":weary: :b: :sparkling_heart: :regional_indicator_i: :sparkling_heart: :regional_indicator_t: :sparkling_heart: :regional_indicator_c: :sparkling_heart: :regional_indicator_h: :weary:",
		":b:",
		":eggplant: :sunglasses: :eggplant: :sunglasses: :eggplant: :sunglasses: :eggplant: :sunglasses: :eggplant:",
		":white_check_mark: :regional_indicator_w: :regional_indicator_i: :regional_indicator_n: :regional_indicator_n: :regional_indicator_e: :regional_indicator_r: :white_check_mark:",
		":alarm_clock: :alarm_clock: :alarm_clock: :alarm_clock: :alarm_clock:",
		":regional_indicator_c: :regional_indicator_o: :regional_indicator_c: :regional_indicator_k: :regional_indicator_s: :exclamation:",
		":punch: :middle_finger: :sun_with_face: :punch: :middle_finger: :sun_with_face: :punch: :middle_finger: :sun_with_face: :punch: :middle_finger: :sun_with_face: :punch: :middle_finger: :sun_with_face:",
	}

	PingsPerDay int

	PingTargets = make(map[string]string)
)

const (
	SecondsPerDay = 60 * 60 * 24
)

func init() {
	rand.Seed(time.Now().UnixNano())

	flag.StringVar(&Token, "t", "", "Bot token")
	flag.StringVar(&WelcomeChan, "w", "", "Welcome channel")
	flag.StringVar(&PingChan, "p", "", "Channel to ping users in")
	flag.IntVar(&PingsPerDay, "i", 100, "Number of times per day to ping")

	flag.Parse()
}

func main() {
	var err error
	// Open the my.db data file in your current directory.
	// It will be created if it doesn't exist.
	DB, err = bolt.Open("dumbot.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal(err)
	}
	defer DB.Close()

	DB.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("ping targets"))
		if b == nil {
			return nil
		}
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			PingTargets[string(k)] = string(v)
			log.Println("Restored", string(k), "from database for ping messages")
		}

		return nil
	})
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)
	dg.AddHandler(guildCreate)

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)

	// Once a second
	var tickerInterval time.Duration = 1 * time.Second
	busTicker := time.NewTicker(tickerInterval).C

	for {
		select {
		case <-sc:
			break

		case <-busTicker:
			if len(PingTargets) == 0 {
				continue
			}
			r := rand.Intn(SecondsPerDay)
			if r <= PingsPerDay*len(PingTargets) {
				i := rand.Intn(len(PingTargets))
				var k string
				for k = range PingTargets {
					if i == 0 {
						break
					}
					i--
				}

				saying := PingSayings[rand.Intn(len(PingSayings))]
				_, err = dg.ChannelMessageSend(PingChan, "Hey "+k+" "+saying)
				if err != nil {
					log.Println("Unable to send ping message to channel", PingChan, ":", err)
				}
			}
			continue
		}
		break
	}

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID || m.Author.Bot {
		return
	}

	fields := strings.Split(m.Content, " ")
	if len(fields) == 0 {
		return
	}

	switch strings.ToLower(fields[0]) {

	// Command that adds someone to the ping list
	case "!ping":

		// If they didn't specify a target, just respond with a pong
		if len(fields) < 2 {
			s.ChannelMessageSend(m.ChannelID, "pong!")
			return
		}

		if fields[1][:2] != "<@" {
			log.Println("Someone wants to annoy", fields[1], "but that's not a user")
			return
		}

		err := DB.Update(func(tx *bolt.Tx) error {
			b, err := tx.CreateBucketIfNotExists([]byte("ping targets"))
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}
			err = b.Put([]byte(fields[1]), []byte(m.Author.ID))
			return err
		})

		if err != nil {
			log.Println("Error adding ping target to db:", err)
			return
		}
		PingTargets[fields[1]] = ""

		s.ChannelMessageSend(m.ChannelID, ":ok_hand:")

		return

		// Command to remove someone from the ping list
	case "!stop":
		if len(fields) < 2 {
			return
		}
		if fields[1][:2] != "<@" {
			log.Println("Someone wants to stop annoying", fields[1], "but that's not a user")
			return
		}
		err := DB.Update(func(tx *bolt.Tx) error {
			b, err := tx.CreateBucketIfNotExists([]byte("ping targets"))
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}
			err = b.Delete([]byte(fields[1]))
			return err
		})

		if err != nil {
			log.Println("Error removing ping target to db:", err)
			return
		}
		delete(PingTargets, fields[1])

		s.ChannelMessageSend(m.ChannelID, ":ok_hand:")

		return
	case "!listpings":
		if len(PingTargets) == 0 {
			s.ChannelMessageSend(m.ChannelID, "No one is currently being bothered")
			return
		}
		var targets = []string{}
		for k, _ := range PingTargets {
			targets = append(targets, k)
		}
		s.ChannelMessageSend(m.ChannelID, "Currently showing love to: "+strings.Join(targets, ", "))
		return
	case "!numpings":
		p, err := strconv.Atoi(fields[1])
		if err != nil {
			log.Println("Error converting", fields[1], "to int:", err)
			return
		}

		PingsPerDay = p

		s.ChannelMessageSend(m.ChannelID, "Now pinging "+fields[1]+" times per day")
		return
	}
}

func guildCreate(s *discordgo.Session, g *discordgo.GuildCreate) {
	s.UpdateStatus(0, "with your heart")
	return

	// this just prints out role IDs
	fmt.Println("\nRoles")
	for _, r := range g.Roles {
		fmt.Println(r.Position, r.Name, r.ID)
	}
	return
}

func hasRole(s *discordgo.Session, guildID string, userID string, roleID string) (bool, error) {
	member, err := s.State.Member(guildID, userID)
	if err != nil {
		if member, err = s.GuildMember(guildID, userID); err != nil {
			return false, err
		}
	}

	// Iterate through the role IDs stored in member.Roles
	// to check permissions
	for _, id := range member.Roles {
		if roleID == id {
			return true, nil
		}
	}

	return false, nil
}

// adds a role if the user doesnt have it, removes it if they do
func swapRoles(s *discordgo.Session, guildID string, userID string, roleID string) (bool, error) {
	dropRole, err := hasRole(s, guildID, userID, roleID)
	if err != nil {
		return false, err
	}
	if dropRole {
		err := s.GuildMemberRoleRemove(guildID, userID, roleID)
		if err != nil {
			return false, err
		}
		return false, nil
	}
	err = s.GuildMemberRoleAdd(guildID, userID, roleID)
	if err != nil {
		log.Println(err)
		return false, err

	}
	return true, nil
}
